import { REGIONS_CONFIG } from "../helpers/config.mjs";

/** RegionsTransferData
 * Standardized data scheme for data being transferred via drag/drop from Regions
 * @param source (string) - identifies the source as Regions.  This should never change.
 * @param sourceRegionID (string) - identifies the Region this data originates from
 * @param internalID (string) - identifies the Regions internal ID of this data
 * @param transferType (string) - identifies the type of transfer this is (ie, Region, Actor, Scene, etc), used by Regions
 * @param type (string) - similar to transferType, utilized by Foundry
 * @param pack (string) - identifies the compendium pack key associated with this data
 * @param object (object) - container for additional data as necessary - such as the entire association information.
 * @param id (string) - Foundry ID (for dragging outside of Regions)
 * @param uuid (string) - Foundry v10 support for id
 */
export class RegionTransferData {
    source = REGIONS_CONFIG.ID;
    sourceRegionID = null;
    internalID = null;
    transferType = null;
    type = null;
    pack = null;
    object = null;
    id = null;
    uuid = null;

    constructor(object) {
        this.sourceRegionID = object?.sourceRegionID;
        this.internalID = this.id = object?.object ? object?.object.id : object?.internalID;
        this.transferType = this.type = object?.transferType;
        this.pack = object?.pack;
        this.object = object?.object;
        this.uuid = `${this.type}.${this.object?.foundryID}`;
    }
}