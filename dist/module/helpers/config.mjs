export const REGIONS_CONFIG = {};

REGIONS_CONFIG.ID = "regions";

REGIONS_CONFIG.FOUNDRYVERSION = 0;

REGIONS_CONFIG.TEMPLATES = {
    REGIONS: `modules/${REGIONS_CONFIG.ID}/templates/regions-manage-regions.hbs`,
    REGION: `modules/${REGIONS_CONFIG.ID}/templates/regions-edit-region.hbs`,
    CONTEXTMENU: `modules/${REGIONS_CONFIG.ID}/templates/context-menu.hbs`,
    EDIT_CUSTOM: `modules/${REGIONS_CONFIG.ID}/templates/regions-edit-custom.hbs`,
    EDIT_TEMPLATE: `modules/${REGIONS_CONFIG.ID}/templates/regions-edit-template.hbs`,
    IMPORT_COMPENDIUM: `modules/${REGIONS_CONFIG.ID}/templates/regions-import-compendium.hbs`,
    MANAGE_BACKUPS: `modules/${REGIONS_CONFIG.ID}/templates/regions-manage-backups.hbs`,
    MANAGE_CATEGORIES: `modules/${REGIONS_CONFIG.ID}/templates/regions-manage-categories.hbs`,
    MANAGE_CUSTOM: `modules/${REGIONS_CONFIG.ID}/templates/regions-manage-custom.hbs`,
    MANAGE_TEMPLATES: `modules/${REGIONS_CONFIG.ID}/templates/regions-manage-templates.hbs`
};

REGIONS_CONFIG.SETTINGS = {
    regions_about: 'region_about',
    region_data: 'region_data',
    region_documents: 'region_documents',
    region_scene_behavior: 'region_scene_behavior',
    regions_backups: 'regions_backups',
    regions_backups_menu: 'regions_backups_menu',
    regions_version: 'regions_version',
    regions_custom_keys: 'regions_custom_keys',
    regions_player_view: 'regions_player_view',
    regions_category: 'regions_category',
    regions_templates: 'regions_templates',
    regions_tags: 'regions_tags'
};

REGIONS_CONFIG.TYPES = {
    SCENE: 'Scene',
    ACTOR: 'Actor',
    ITEM: 'Item',
    JOURNAL: 'JournalEntry',
    ROLLTABLE: 'RollTable',
    PLAYLIST: 'Playlist',
    COMPENDIUM: 'Compendium',
    CUSTOM: 'Custom',
    REGION: 'Region',
    TEMPLATE: 'Template',
    FOLDER: 'Folder'
};

REGIONS_CONFIG.KEY_TYPES = {
    TEXT: "Text",
    TOGGLE: "Toggle",
    LIST: "List",
    COUNTER: "Counter",
    NUMERIC: "Numeric",
    SLIDER: "Slider"
};

REGIONS_CONFIG.CATEGORY_TYPES = {
    REGION: "Region",
    ASSOCIATION: "Association"
};

REGIONS_CONFIG.CODES = {
    OK: 0,
    INTERNAL_ERROR: 1,
    INVALID_REQUEST: 2,
    NOT_FOUND: 3
}

REGIONS_CONFIG.DEFAULTS = {
    region_data: [],
    region_documents: {customTags: [], associations: [], categories: [], templates: []},
    regions_cateogry: [],
    region_scene_behavior: false,
    regions_player_view: false
}