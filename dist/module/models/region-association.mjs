import { REGIONS_CONFIG } from "../helpers/config.mjs";
import { RegionsData } from "../helpers/data.mjs";
import { Logger } from "../geekdialog/geekdialogs.mjs";

/**
 * @typedef {Object} RegionAssociation 
 * @property {string} type - what type of entity this association is
 * @property {string} id - Internal id of this association
 * @property {string} foundryID - Foundry's internal id.  Can be null.
 * @property {string} compendiumKey - The compendium this association is part of.  Can be null
 * @property {string} compendiumID - The known compendium ID this association has.  Can be null
 * @property {string} foundryIMG - The image path/data used by Foundry
 * @property {string} foundryName - The name of the object in Foundry
 * @property {string} regionID - The id of the region association with this object
 * @property {string} description - A brief description shown in the Regions UI
 * @property {string} categoryID - The category ID that this association belongs to.
 * @property {object} customData - data available for custom tags.
 * @property {number} sortOrder - Order priority of this association.  Lower means on display first.
 */
export class RegionAssociation {
    constructor(defaults = {}) {
        this.id = defaults.id ?? foundry.utils.randomID(16);
        this.type = defaults.type ?? REGIONS_CONFIG.TYPES.CUSTOM;
        this.foundryID = defaults.foundryID ?? null;
        this.compendiumKey = defaults.compendiumKey ?? null;
        this.compendiumID = defaults.compendiumID ?? null;
        this.foundryIMG = defaults.foundryIMG ?? null;
        this.foundryName = defaults.foundryName ?? null;
        this.regionID = defaults.regionID ?? null;
        this.description = defaults.description ?? null;
        this.categoryID = defaults.categoryID ?? null;
        this.customData = defaults.customData ?? {};
        this.sortOrder = defaults.sortOrder ?? 10;
    }

    /**
     * buildAssociationFromData
     * @param {*} region - RegionInfo object
     * @param {*} data - Expected Structure:
     * {
     *  id?
     *  type?
     *  pack?
     *  name?
     *  transferType?
     *  sourceRegionID?
     *  object?
     *  data.thumb?
     * }
     * @param {*} categoryID 
     * @returns 
     */
    static async buildAssociationFromData(region, data, categoryID) {
        Logger.debug(false, "Building new association:", region, data, categoryID);

        let association = new RegionAssociation();
        if (data.source === "regions") {
            association = new RegionAssociation(data.object ? data.object : data);
            association.regionID = region.id;
        } else if (data.type === "Compendium") {
            association.foundryID = data.id;
            association.type = data.type;
            association.regionID = region.id;
        } else {
            if (data.object) {
                association.id = data.object?.id ?? data.object.id;
            }
            association.foundryID = data.uuid.replace(`${data.type}.`, "") ?? data.id;
            association.type = data.type ?? data.transferType;
            association.compendiumKey = data.pack;
            association.regionID = region.id;
    
            if (data.uuid && data.uuid.startsWith("Compendium")) {
                let parts = data.uuid.split('.');
                association.foundryID = parts[parts.length-1];
                association.compendiumKey = data.uuid.replace(parts[0]+".", "").replace("." + parts[parts.length - 1], "");
            }
        }

        let tabName = null;
        let pack = null;
        let isExisting = false;
        if (association.compendiumKey) {
            pack = game.packs.get(association.compendiumKey.split(".").slice(0, -1).join("."));
        } else if (association.type === REGIONS_CONFIG.TYPES.COMPENDIUM) {
            pack = game.packs.get(association.foundryID)
        }

        let associations = foundry.utils.duplicate(region.associations);
        let existingAssociation = associations.find(a => a.foundryID === association.foundryID && a.type === association.type);
        switch (association.type) {
            case REGIONS_CONFIG.TYPES.SCENE:
                tabName = "scenes";
                let s = pack ? await pack.getDocument(association.foundryID) : game.scenes.get(association.foundryID);
                association.foundryIMG = s.thumb ?? "icons/fvtt.png",
                association.foundryName = s.name;
                break;
            case REGIONS_CONFIG.TYPES.ACTOR:
                tabName = "actors";
                let a = pack ? await pack.getDocument(association.foundryID) : game.actors.get(association.foundryID);
                association.foundryIMG = a.img;
                association.foundryName = a.name;
                break;
            case REGIONS_CONFIG.TYPES.ITEM:
                tabName = "items";
                let i = pack ? await pack.getDocument(association.foundryID) : game.items.get(association.foundryID);
                association.foundryIMG = i.img;
                association.foundryName = i.name;
                break;
            case REGIONS_CONFIG.TYPES.JOURNAL:
                tabName = "journals";
                let j = pack ? await pack.getDocument(association.foundryID) : game.journal.get(association.foundryID);
                association.foundryIMG = j.img ?? "icons/svg/book.svg";
                association.foundryName = j.name;
                break;
            case REGIONS_CONFIG.TYPES.ROLLTABLE:
                tabName = "rolltables";
                let t = pack ? await pack.getDocument(association.foundryID) : game.tables.get(association.foundryID);
                association.foundryIMG = t.img ?? "icons/vtt.png";;
                association.foundryName = t.name;
                break;
            case REGIONS_CONFIG.TYPES.PLAYLIST:
                tabName = "playlists";
                let p = pack ? await pack.getDocument(association.foundryID) : game.playlists.get(association.foundryID);
                association.foundryIMG = "icons/svg/sound.svg";
                association.foundryName = p.name;
                break;
            case REGIONS_CONFIG.TYPES.REGION:
                tabName = "regions";
                if (data.sourceRegionID) {
                    let r = await RegionsData.getRegion(data.sourceRegionID);
                    association.foundryID = data.sourceRegionID;
                    association.foundryIMG = r.icon;
                    association.foundryName = r.name;
                } else {
                    association = data.object;
                }
                break;
            case REGIONS_CONFIG.TYPES.CUSTOM:
                tabName = "custom";
                existingAssociation = associations.find(a => a.type === REGIONS_CONFIG.TYPES.CUSTOM && a.id === data.object?.id);
                if (!existingAssociation) {
                    association.customData = data.object.customData ? foundry.utils.duplicate(data.object.customData) : foundry.utils.duplicate(data.object);
                    association.customData.id = association.id = foundry.utils.randomID(16);
                }
                break;
            case REGIONS_CONFIG.TYPES.COMPENDIUM:
                tabName = "compendiums";
                association.foundryIMG = pack.banner;
                association.foundryName = pack.title;
                break;
            default:
                break;
        }

        if (existingAssociation) {
            if (existingAssociation.categoryID !== categoryID) {
                association = foundry.utils.duplicate(existingAssociation);
                association.categoryID = categoryID;
            }
            isExisting = true;
        } else {
            association.categoryID = categoryID;
        }

        return { association, tabName, isExisting: isExisting };
    }

    async getAssociationDocument() {
        let doc = null;

        if (this.compendiumKey) {
            const pack = game.packs.get(this.compendiumKey.split(".").slice(0, -1).join("."));
            if (pack) { doc = await pack.getDocument(this.foundryID); }
        }

        if (!doc) {
            switch (this.type) {
                case REGIONS_CONFIG.TYPES.SCENE:
                    doc = game.scenes.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.ACTOR:
                    doc = game.actors.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.ITEM:
                    doc = game.items.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.JOURNAL:
                    doc = game.journal.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.ROLLTABLE:
                    doc = game.tables.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.PLAYLIST:
                    doc = game.playlists.get(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.REGION:
                    doc = await RegionsData.getRegion(this.foundryID);
                    break;
                case REGIONS_CONFIG.TYPES.COMPENDIUM:
                    doc = game.packs.get(this.foundryID);
                    break;
                default:
                    break;
            }
        }

        return doc;
    }

    async renderAssociationSheet() {
        if (this.type === REGIONS_CONFIG.TYPES.REGION) {
            this.viewRegion();
            return;
        }
        
        let doc = await this.getAssociationDocument();
        let sheet = doc?.sheet;
        if (sheet) {
            sheet.render(true);
        } else {
            doc.render(true);
        }
    }

    async viewScene() {
        if (this.type !== REGIONS_CONFIG.TYPES.SCENE) {
            return;
        }

        let scene = await this.getAssociationDocument();
        let region = await RegionsData.getRegion(this.regionID);

        if (this.compendiumKey) {
            //Check if this scene has been imported already
            let pack = game.packs.get(this.compendiumKey);

            let sceneCheck = game.scenes.getName(this.foundryName);
            if (!sceneCheck) {
                scene = await game.scenes.importFromCompendium(pack, this.foundryID, {folder: region.folders.sceneFolder});
            } else {
                scene = sceneCheck;
            }
        }

        let activateScene = game.settings.get(REGIONS_CONFIG.ID, REGIONS_CONFIG.SETTINGS.region_scene_behavior);
        if (activateScene) {
            scene.activate();
        } else {
            scene.view();
        }
    }

    async viewRegion() {
        if (this.type !== REGIONS_CONFIG.TYPES.REGION) {
            return;
        }

        let region = await this.getAssociationDocument();
        region.renderRegion();
    }

    async rollTable() {
        if (this.type !== REGIONS_CONFIG.TYPES.ROLLTABLE) {
            return;
        }

        let table = await this.getAssociationDocument();
        table?.draw();
    }

    async play() {
        if (this.type !== REGIONS_CONFIG.TYPES.PLAYLIST) {
            return;
        }

        let playlist = await this.getAssociationDocument();
        playlist?.playAll();
    }

    async hasPermission() {
        let doc = null;
        if (game.user.isGM) { return true; }
        if (this.compendiumKey) {
            let pack = game.packs.get(this.compendiumKey);
            if (pack.private) { return false; }
        }
        if (this.type === REGIONS_CONFIG.TYPES.REGION) {
            let r = RegionsData.getAllRegions()?.find(r => r.id === this.foundryID);
            return (!r) ? false : !r.private;
        }

        doc = await this.getAssociationDocument();
        if (doc) {
            return doc.permission > 0;
        } else {
            return false;
        }
    }
}