export class RegionsCategory {
    id = null;
    name = null;
    type = null;

    constructor(type = null) {
        this.id = foundry.utils.randomID(16);
        this.type = type;
        this.name = game.i18n.localize("REGIONS.new-category")
    }
}